
describe('Module Test', () => {
  beforeEach(() => {
    cy.visit('/')})

    in('checks basic existence', () => {
      cy.get('.main').should('exist')
      cy.get('.header-main').should('exist')
      cy.get('.middle').should('exist')
      cy.get('.reject-approve-box').should('exist')
      cy.get('.skeleton-main-box').should('not.exist')
    })
    
    it('slot context and item-box menus should not exist by default', () => {
      cy.get('.slot-context-box').should('not.exist')
      cy.get('.item-box').should('not.exist')
    })

    it('checks if upon clicking options DropdownButton slot is visible', () => {
      cy.get('[style="display: table;"]').eq(0).click()
      .get('.item-box').should('be.visible')
    })

    it('checks if by default both Approve and Reject buttons are visible', () => {
      cy.get('.button-box').eq(5).should('be.visible')
      cy.get('.button-box').eq(6).should('be.visible')
    })

    it('checks if after pressing Approve button, it gets removed', () => {
      cy.get('.button-box').eq(5).click().should('not.exist')
    })

    it('checks if after pressing Reject button, it gets removed', () => {
      cy.get('.button-box').eq(6).click().should('not.exist')
    })

    it('checks if Approved DropdownButton context is visible after clicking Approve button, then Approved DropdownButton ', () => {
      cy.get('.button-box').last().click()
      .get('[style="display: table;"]').eq(1).click()
      .get('.slot-context-box').should('be.visible')
    })
    
    it('checks if Rejected DropdownButton context is visible after clicking Rejected button, then Rejected DropdownButton ', () => {
      cy.get('.button-box').eq(5).click()
      .get('[style="display: table;"]').last().click()
      .get('.slot-context-box').should('be.visible')
    })

})
